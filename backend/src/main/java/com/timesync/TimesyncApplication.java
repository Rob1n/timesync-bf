package com.timesync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimesyncApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimesyncApplication.class, args);
	}
}
