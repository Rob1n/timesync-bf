package com.timesync.controller;

import com.timesync.data.dao.UserAccountDAO;
import com.timesync.data.dao.UserGroupDAO;
import com.timesync.service.UserAccountService;
import com.timesync.service.UserGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping(value = "/users")
public class UserAccountController {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private UserAccountService userAccountService;

	@Autowired
	public UserAccountController(UserAccountService userAccountService, UserGroupService userGroupService) {
		this.userAccountService = userAccountService;
	}

	@GetMapping
	public List<UserAccountDAO> findAllUsers() {
		return userAccountService.findAll();
	}

	@GetMapping(value = "/{userId}")
	public UserAccountDAO findUser(@PathVariable("userId") Long userId) {
		return userAccountService.findById(userId);
	}

	@PostMapping(value = "/add")
	public UserAccountDAO addNewUser(@RequestBody UserAccountDAO dao) {
		return userAccountService.save(dao);
	}

	@GetMapping(value = "/{userId}/delete")
	public void deleteUser(@PathParam("userId") Long userId) {
		userAccountService.deleteById(userId);
	}

	@GetMapping(value = "/{userId}/groups")
	public List<UserGroupDAO> getUserGroups(@PathVariable("userId") Long userId) {
		return userAccountService.findById(userId).getGroups();
	}

}
