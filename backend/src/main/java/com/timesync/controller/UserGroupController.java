package com.timesync.controller;

import com.timesync.data.dao.UserAccountDAO;
import com.timesync.data.dao.UserGroupDAO;
import com.timesync.service.UserAccountService;
import com.timesync.service.UserGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/groups")
public class UserGroupController {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private UserGroupService userGroupService;
	private UserAccountService userAccountService;

	@Autowired
	public UserGroupController(UserGroupService userGroupService, UserAccountService userAccountService) {
		this.userGroupService = userGroupService;
		this.userAccountService = userAccountService;
	}

	@GetMapping
	public List<UserGroupDAO> findAll() {
		return userGroupService.findAll();
	}

	@GetMapping(value = "/{groupId}")
	public UserGroupDAO find(@PathVariable("groupId") Long groupId) {
		return userGroupService.findById(groupId);
	}

	@PostMapping(value = "/add")
	public UserGroupDAO addNewGroup(@RequestBody UserGroupDAO dao) {
		return userGroupService.save(dao);
	}

	@GetMapping(value = "/{userGroup}/delete")
	public void deleteUserGroup(@PathVariable("userGroup") Long id) {
		userGroupService.deleteById(id);
	}

	@GetMapping(value = "/{userGroup}/addUser/{userId}")
	public UserGroupDAO addUserToGroup(@PathVariable("userGroup") Long userGroupId, @PathVariable("userId") Long userId) {
		UserAccountDAO user = userAccountService.findById(userId);
		return userGroupService.addUserToGroup(userGroupId, user);
	}

}
