package com.timesync.data.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class GroupNotFoundException extends RuntimeException {

	public GroupNotFoundException(Long groupId) {
		super("could not find group with id'" + groupId + "'.");
	}

	public GroupNotFoundException(String groupName) {
		super("could not find group '" + groupName + "'.");
	}
}