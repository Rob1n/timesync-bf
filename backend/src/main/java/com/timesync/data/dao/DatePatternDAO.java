package com.timesync.data.dao;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
@Table(name = "DATE_PATTERN")
public class DatePatternDAO {

	public enum REPEAT_STRATEGY {
		DAILY, WEEKLY, MONTHLY;
	}

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "START_DATE")
	@NotNull
	private Instant date;

	@Column(name = "REPEAT_STRATEGY")
	@Enumerated(EnumType.STRING)
	private REPEAT_STRATEGY repeatStrategy;

	@Column(name = "FREQUENCY")
	private int frequency;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Instant getDate() {
		return date;
	}

	public void setDate(Instant date) {
		this.date = date;
	}

	public REPEAT_STRATEGY getRepeatStrategy() {
		return repeatStrategy;
	}

	public void setRepeatStrategy(REPEAT_STRATEGY repeatStrategy) {
		this.repeatStrategy = repeatStrategy;
	}
}
