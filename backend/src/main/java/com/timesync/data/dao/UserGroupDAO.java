package com.timesync.data.dao;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "USER_GROUP")
public class UserGroupDAO {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "ADMINISTRATOR_ID", nullable = false)
	private UserAccountDAO administrator;

	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "groups")
	private List<UserAccountDAO> users;

	@Column(name = "GROUP_DESCRIPTION")
	@NotNull
	@JsonProperty("group_description")
	private String groupDescription;

	@Column(name = "DATE_CREATED")
	private Instant dateCreated;

	@Column(name = "NEXT_COMMON_DATE")
	private Instant nextCommonDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<UserAccountDAO> getUsers() {
		return users;
	}

	public void setUsers(List<UserAccountDAO> users) {
		this.users = users;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public Instant getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Instant dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Instant getNextCommonDate() {
		return nextCommonDate;
	}

	public void setNextCommonDate(Instant nextCommonDate) {
		this.nextCommonDate = nextCommonDate;
	}
}
