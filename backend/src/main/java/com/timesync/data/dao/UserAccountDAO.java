package com.timesync.data.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "USER_ACCOUNT")
public class UserAccountDAO {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "USERNAME")
	@NotNull
	private String username;

	@Column(name = "PASSWORD")
	@NotNull
	private String password;

	@Column(name = "EMAIL")
	@Email
	@NotNull
	private String email;

	@Column(name = "DATE_CREATED")
	@JsonProperty(value = "date_created")
	@NotNull
	private Instant dateCreated;

	@OneToMany(mappedBy = "administrator")
	private List<UserGroupDAO> groupsAdministrated;

	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "USER_ACCOUNT_GROUP", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "group_id")})
	@JsonIgnore
	private List<UserGroupDAO> groups;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Instant getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Instant dateCreated) {
		this.dateCreated = dateCreated;
	}

	public List<UserGroupDAO> getGroups() {
		return groups;
	}

	public void setGroups(List<UserGroupDAO> groups) {
		this.groups = groups;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonProperty(value = "password")
	public void setPassword(String password) {
		this.password = password;
	}
}


