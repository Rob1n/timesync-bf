package com.timesync.data.dao

class Greeting {

    data class Greeting(val id: Long, val content: String)

}