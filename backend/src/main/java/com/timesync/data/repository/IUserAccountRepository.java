package com.timesync.data.repository;

import com.timesync.data.dao.UserAccountDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IUserAccountRepository extends JpaRepository<UserAccountDAO, Long> {
	Optional<UserAccountDAO> findByUsername(String username);
}
