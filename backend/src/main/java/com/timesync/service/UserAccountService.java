package com.timesync.service;

import com.timesync.data.dao.UserAccountDAO;
import com.timesync.data.exception.UserNotFoundException;
import com.timesync.data.repository.IUserAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserAccountService {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private	IUserAccountRepository repository;

	@Autowired
	public UserAccountService(IUserAccountRepository repository) {
		this.repository = repository;
	}

	public UserAccountDAO findById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new UserNotFoundException(id));
	}

	public UserAccountDAO findByUsername(String username) {
		return repository.findByUsername(username)
				.orElseThrow(() -> new UserNotFoundException(username));
	}

	public List<UserAccountDAO> findAll() {
		return repository.findAll();
	}

	public UserAccountDAO save(UserAccountDAO dao) {
		dao.setDateCreated(new Date().toInstant());
		return repository.save(dao);
	}

	public void deleteById(UserAccountDAO dao) {
		repository.delete(dao);
	}

	public void deleteById(Long id) {
		repository.deleteById(id);
	}

}
