package com.timesync.service;

import com.timesync.data.dao.UserAccountDAO;
import com.timesync.data.dao.UserGroupDAO;
import com.timesync.data.exception.GroupNotFoundException;
import com.timesync.data.repository.IUserGroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserGroupService{
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private IUserGroupRepository repository;

	@Autowired
	public UserGroupService(IUserGroupRepository repository) {
		this.repository = repository;
	}

	public UserGroupDAO findById(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new GroupNotFoundException(id));
	}

	public List<UserGroupDAO> findAll() {
		return repository.findAll();
	}

	public UserGroupDAO save(UserGroupDAO dao) {
		return repository.save(dao);
	}

	public void delete(UserGroupDAO dao) {
		repository.delete(dao);
	}

	public void deleteById(Long id) {
		repository.deleteById(id);
	}

	public UserGroupDAO addUserToGroup(Long groupId, UserAccountDAO user) {
		UserGroupDAO group = findById(groupId);
		group.getUsers().add(user);
		return save(group);
	}

}
